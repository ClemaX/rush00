/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   rush03.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chamada <marvin@le-101.fr>                 +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/03 17:43:14 by chamada      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/04 18:17:59 by nigoncal    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_write_sandwich(char *chars, int size)
{
	int i;

	i = 0;
	ft_putchar(chars[0]);
	while (i < size - 2)
	{
		ft_putchar(chars[1]);
		i++;
	}
	if (size > 1)
		ft_putchar(chars[2]);
	ft_putchar('\n');
}

void	rush(int n_x, int n_y)
{
	int i;

	i = 0;
	if (n_y < 1 | n_x < 1)
		return ;
	ft_write_sandwich("o-o", n_x);
	while (i < n_y - 2)
	{
		ft_write_sandwich("| |", n_x);
		i++;
	}
	if (n_y > 1)
		ft_write_sandwich("o-o", n_x);
}
